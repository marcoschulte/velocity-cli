# Use Apache Velocity from your command line
A few lines of code to use the Apache Velocity template engine from the command line. Handy if you want to create some static html pages.

## Usage
Call the script with `velocity-cli NEWSUFFIX FILES...`

Velocity syntax reference: http://velocity.apache.org/engine/releases/velocity-1.7/user-guide.html

### Example 1
taken from the velocity user guide

    <html>
    <body>
      #set( $foo = "Velocity" )
      Hello $foo World!
    </body>
    <html>

Parse template with `velocity-cli html helloworld.vm`

### Example 2
**layout.vm**

	<!DOCTYPE html>
	<html>
	  <head>
		<title>$title</title>
		<style>
			.active {font-weight: bold;}
		</style>
	  </head>
	<body>
	  <nav>
	#foreach ($i in [1..2])
		<a href="page${i}.html" class="#if($page==$i)active#end">Page ${i}</a>
	#end
	  </nav>
	  
	$content
	
	</body>

**page1.vm**

	#set($title="Title of page 1")
	#set($page=1)
	#define( $content )
	<h1>Hello World</h1>
	
	I am page 1!
	
	#end 
	#parse("layout.vm")

**page2.vm**

	#set($title="Title of page 2")
	#set($page=2)
	#define( $content )
	<h1>Hello World</h1>
	
	I am page 2!
	
	#end 
	#parse("layout.vm")

Parse both pages with `velocity-cli html page*.vm`

## Build
Build with `mvn assembly:single`

Add alias `alias velocity-cli='java -jar /.../velocity-cli.jar'`