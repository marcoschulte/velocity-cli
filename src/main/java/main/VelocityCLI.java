package main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import org.apache.commons.lang.CharEncoding;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

public class VelocityCLI {

	public static boolean checkArgs(String[] args) {
		if (args.length < 2) {
			return false;
		}

		for (int i = 1; i < args.length; i++) {
			File f = new File(args[i]);
			if (!f.exists()) {
				System.out.println(String.format("File %s not existing",
						f.getAbsoluteFile()));
				return false;
			}
		}

		return true;
	}

	public static void main(String[] args) throws Exception {
		if (!checkArgs(args)) {
			System.err.println("Usage: velocity-cli NEWSUFFIX FILES...");
			return;
		}

		String suffix = args[0];

		Velocity.init();
		VelocityContext context = new VelocityContext();

		for (int i = 1; i < args.length; i++) {
			String filename = args[i];
			String outfile = (new File(filename)).getName();
			outfile = outfile.substring(0, outfile.lastIndexOf('.') + 1)
					+ suffix;

			System.out.println(String.format("Processing %s, writing to %s",
					filename, outfile));

			Template template = Velocity.getTemplate(filename,
					CharEncoding.UTF_8);
			OutputStreamWriter out = new OutputStreamWriter(
					new FileOutputStream(outfile), CharEncoding.UTF_8);
			template.merge(context, out);

			out.close();
		}
	}
}
